# ImAlive

Sends timestamp to database to report app is alive.

## Usage

Install with npm

```
npm install --save imalive
```

```javascript
var imalive = require("imalive")

var options = {
    Hostname    : 'somehostname.com',
    DBName      : 'mydatabase',
    Dialect     : '', /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
    Port        : 3306, /* 3306 is default port for mySql. Change it upon your needs */
    User        : 'myUser',
    Password    : 'myPassword',
    Table       : 'TableForMonitoringPurpose',
    ServiceName : 'ServiceReportingAliveState',
    Interval    : 5000, /* Optional. Nb. of ms between updates. If not specified, update will occur once */
    Logging     : true, /* Optional. If specified and true, detailed logging will be outputed to console. Otherwise, only errors will be outputed. */
    NbAddColumns: 3 /* Optional. If your destination table contains additional columns, specifie the number here, to make the insert command working when creating a new entry (Inserted values will be null, so columns must be nullable). */
};

imalive(
    options,
    function(err) {
        if (err) {
            console.error('ERROR: ', err);
        }
        else {
            console.log('SUCCESS!');
        }
    }
);

```

## Table Definition

Table into which status is reported MUST be defined as following (Table name is up to you since it is defined into options):

Column ID | Column name | Definition / Comments
----------|-------------|----------------------
1 | ID | PKey, int, Auto_Increment
2 | ServiceName | unique, varchar(50)
3 | HeartBeat | datetime
n | Up to you | Up to you

_Note: Columns 1 to 3 MUST have these names and definitions. Remaining columns names and definition are up to you. If you have additional columns, the number must be specified in options (parameter NbAddColumns). Otherwise, the initial INSERT (when adding a new service) will fail. These additional columns must be nullable in the database._


## Behaviour of options.Interval

When an interval is specified and greater than zero (0), the module itself will send update requests to the database every _nnnn_ milliseconds.
If you prefer to control your update interval by yourself, just don't specify this option of set it to zero (0), and the module will send only one update request.
Resist to the temptation to put a value smaller than 1000 (1 second) because the database will possibly reject most of your requests. Use a value that fits your needs, but since this is a database update, even if it is very light, the traffic, and the opening/closing connection action, could become an issue if this value is too small. Evaluate your needs to put the biggest possible value.


## Testing status

Tested only with MySql/MariaDB database for the moment.


## Upcoming changes

Raw queries will probably be changed for SequelizeJS models in the future.
