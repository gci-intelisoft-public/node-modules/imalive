/* jshint esversion: 6 */
const Sequelize = require('sequelize');

function imAlive(options, callback) {

    if (options.Interval)
        if (options.Logging) console.log("Updating now. Next update in", options.Interval, "ms.");

    process.env.db_timezone = 'Etc/GMT0';

    var sequelize = new Sequelize(options.DBName, options.User, options.Password, {
        host: options.Hostname,
        dialect: options.Dialect,
        port: options.Port,
        dialectOptions: {
            collate: 'utf8mb4_general_ci',
            useUTC: true,
            timezone: process.env.db_timezone
        },
        benchmark: false,
        logging: false
      });

    sequelize
        .authenticate()
        .then(() => {
            if (options.Logging) console.log("Database authentication successful!");
            var tablename = options.Table;
            var servicename = options.ServiceName;
            var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
            var date = new Date(Date.now() - tzoffset);
            // Extract the date and time portions from ISO formatted date
            var nowD = date.toISOString().split('T')[0];
            var nowT = date.toISOString().split('T')[1].split('.')[0];
            var now = nowD.concat(' ').concat(nowT);

            sequelize.query("SELECT * FROM `" + tablename + "` WHERE ServiceName = '" + servicename + "'", { type: sequelize.QueryTypes.SELECT })
                .then(serviceResult => {
                    if (serviceResult.length == 0) {
                        if (options.Logging) console.log("Service", servicename, "doesn't exist in the database. Creating it.");
                        var addColumns = "";
                        if (options.NbAddColumns) {
                          for (var col = 0; col < options.NbAddColumns; col++) {
                            addColumns += ", null";
                          }
                        }
                        sequelize.query("INSERT INTO `" + tablename + "` VALUES (null, '" + servicename + "', '" + now + "'" + addColumns + ")", { type: sequelize.QueryTypes.INSERT })
                            .then(([results, metadata]) => {
                                sequelize.close()
                                    .then(() => {
                                        if (options.Logging) console.log("Connection to database is now closed.");
                                    })
                                    .catch(err => {
                                        console.error("Error disconnecting from database. " + err);
                                    });
                                if (metadata == 0) {
                                    console.error("Insertion failed. Service", servicename, "couldn't be created!");
                                    callback('Data insert failed');
                                }
                                else {
                                    if (options.Logging) console.log("Service", servicename, "created!");
                                    callback();
                                }
                            });
                    }
                    else {
                        if (options.Logging) console.log("Service", servicename, "exists in the database. Updating its heartbeat.");
                        var ID = serviceResult[0].ID;
                        sequelize.query("UPDATE `" + tablename + "` SET HeartBeat = '" + now + "' WHERE ID = '" + ID + "'", { type: sequelize.QueryTypes.UPDATE })
                            .then(([results, metadata]) => {
                                sequelize.close()
                                    .then(() => {
                                        if (options.Logging) console.log("Connection to database is now closed.");
                                    })
                                    .catch(err => {
                                        console.error("Error disconnecting from database. " + err);
                                    });
                                if (metadata == 0) {
                                    console.error("Update failed. Service", servicename, "heartbeat couldn't be updated!");
                                    callback('Data update failed');
                                }
                                else {
                                    if (options.Logging) console.log("Service", servicename, "heartbeat updated!");
                                    callback();
                                }
                            });
                    }
                });
        })
        .catch(err => {
            console.error("Database authentication failed!");
            sequelize.close()
                .then(() => {
                    if (options.Logging) console.log("Connection to database is now closed.");
                })
                .catch(err => {
                    console.error("Error disconnecting from database. ", err);
                });
            callback('Unable to connect to the database: ' + err);
        });

    if (options.Interval) {
        setTimeout(() => imAlive(options, callback), options.Interval);
    }
}

module.exports = imAlive;
